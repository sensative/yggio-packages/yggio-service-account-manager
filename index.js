'use strict';

const get = require('lodash.get');
const delay = require('delay');
const yggioApi = require('yggio-api');

const redundantLogin = (serviceAccountInfo) => {
  console.log('redundantLogin user: ', serviceAccountInfo);
  return yggioApi.register(serviceAccountInfo)
    .catch(err => {
      console.warn('yggio-subscriber: failed to register, trying to login instead');
      return yggioApi.login(serviceAccountInfo);
    })
    .then(serviceAccount => {
      const token = get(serviceAccount, 'token', '');
      console.info('Register/Login token: ', token);
      return Promise.resolve(serviceAccount);
    })
    .catch(err => {
      const retryDelay = get(serviceAccountInfo, 'retryDelay', 5000);
      console.warn('Register/Login failed: ' + err.message + '. Retrying in ' + retryDelay + ' ms');
      return delay(retryDelay).then(() => {
        redundantLogin(serviceAccountInfo);
      });
    });
};

module.exports = {
  connect: redundantLogin
};
