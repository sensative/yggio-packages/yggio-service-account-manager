## yggio-service-account-manager: A 'login service' for yggio services

# This is being replaced with 'yggio-account'

to register a new service provider, an yggio-user is needed. This module encapsulates
the stuff necessary to use "user information" to login/register that yggio-user.

### Usage

```javascript
const serviceAccountManager = require('yggio-service-account-manager');
// you need a config with the correct stuffs
const serviceAccountInfo = {
  username: 'my-new-service@test.com',
  password: 'so-secret-so-secret'
  retryDelay: 7000 // ms (if !retryDelay, default is 5000 ms)
};
return serviceAccountManager.connect(serviceAccountInfo)
  .then(serviceAccount => {
    // typically: create/retrieve the service provider now that we
    // have our serviceAccount. And other setup stuff.
  });
```
### License
yggio-service-account-manager is under the MIT license. See LICENSE file.
